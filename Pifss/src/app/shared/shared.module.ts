import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './components/layout/nav/nav.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NavComponent, HeaderComponent, FooterComponent],
  exports: [
    NavComponent,
    HeaderComponent,
    FooterComponent
  ]
})
export class SharedModule { }
